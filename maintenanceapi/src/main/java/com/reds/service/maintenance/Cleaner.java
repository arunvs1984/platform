package com.reds.service.maintenance;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Cleaner {

	public void removeUnUsedDependencies(String libFiles) throws IOException {
		if(!new File(libFiles).exists()) {
			System.out.println("No Files To Remove");
			return ;
		}
		
		String libraries = new String(Files.readAllBytes(Paths.get(libFiles)));
		String libArray[] = libraries.split(",");
		for (String lib : libArray) {
			String path="../"+lib;
			boolean deleteStatus=Files.deleteIfExists(new File(path).toPath());			
			System.out.println("Deleted "+ path+ " > "+deleteStatus);
		}
	}
	
	public void removeTemp(String tmpMetaData) throws IOException {
		if(!new File(tmpMetaData).exists()) {
			System.out.println("No Temp Files To Remove");
			return ;
		}
		
		//TODO
	}

}

package com.reds.service.maintenance;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class MaintenanceImpl implements Maintenance {

	private static String libFiles = "./removables.txt";
	private static String tmplFiles = "./removables.txt";

	public static void main(String[] args) throws IOException {
		Cleaner libraryMaintenance = new Cleaner();
		libraryMaintenance.removeUnUsedDependencies(libFiles);
		Files.deleteIfExists(new File(libFiles).toPath());
		System.out.println("Maintenance Process Completed.......");
	}
	

}

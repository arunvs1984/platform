package com.reds.platform.platformmanagement;

import dagger.internal.Factory;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementManager_Factory implements Factory<PlatformmanagementManager> {
  private static final PlatformmanagementManager_Factory INSTANCE =
      new PlatformmanagementManager_Factory();

  @Override
  public PlatformmanagementManager get() {
    return provideInstance();
  }

  public static PlatformmanagementManager provideInstance() {
    return new PlatformmanagementManager();
  }

  public static PlatformmanagementManager_Factory create() {
    return INSTANCE;
  }

  public static PlatformmanagementManager newPlatformmanagementManager() {
    return new PlatformmanagementManager();
  }
}

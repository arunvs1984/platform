package com.reds.platform.platformmanagement.command;

import dagger.internal.Factory;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class CommandControlImpl_Factory implements Factory<CommandControlImpl> {
  private static final CommandControlImpl_Factory INSTANCE = new CommandControlImpl_Factory();

  @Override
  public CommandControlImpl get() {
    return provideInstance();
  }

  public static CommandControlImpl provideInstance() {
    return new CommandControlImpl();
  }

  public static CommandControlImpl_Factory create() {
    return INSTANCE;
  }

  public static CommandControlImpl newCommandControlImpl() {
    return new CommandControlImpl();
  }
}

package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.PlatformmanagementManager;
import com.reds.platform.platformmanagement.command.CommandControl;
import com.reds.platform.platformmanagement.command.CommandShell;
import com.reds.platform.platformmanagement.command.cmd.PlatformCommand;
import com.reds.platform.platformmanagement.config.ConfigurationManager;
import com.reds.platform.platformmanagement.info.PlatformmanagementInfo;
import com.reds.platform.platformmanagement.service.ServiceManager;
import com.reds.platform.platformmanagement.service.ServiceRegistry;
import com.reds.platform.platformmanagement.service.persistance.ServiceDependencyPersistence;
import com.reds.platform.platformmanagement.service.persistance.ServiceRegistryPersistance;
import com.reds.platform.redsplatformcommons.command.executor.PlatformCommandExecutor;
import com.reds.platform.redsplatformcommons.command.executor.ServiceCommandExecutor;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerPlatformmanagementComponent implements PlatformmanagementComponent {
  private PlatformmanagementModule platformmanagementModule;

  private Provider<PlatformmanagementManager> providePlatformmanagementManagerProvider;

  private Provider<ConfigurationManager> provideConfigurationManagerProvider;

  private Provider<CommandControl> provideCommandControlProvider;

  private Provider<PlatformCommandExecutor> providePlatformCommandExecutorProvider;

  private Provider<CommandShell> provideCommandShellProvider;

  private Provider<PlatformCommand> providePlatformCommandProvider;

  private Provider<ServiceRegistry> provideServiceRegistryProvider;

  private Provider<ServiceManager> provideServiceManagerProvider;

  private Provider<ServiceCommandExecutor> provideServiceCommandExecutorProvider;

  private Provider<ServiceRegistryPersistance> provideServiceRegistryPersistanceProvider;

  private Provider<ServiceDependencyPersistence> provideServiceDependencyPersistenceProvider;

  private DaggerPlatformmanagementComponent(Builder builder) {
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static PlatformmanagementComponent create() {
    return new Builder().build();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {
    this.platformmanagementModule = builder.platformmanagementModule;
    this.providePlatformmanagementManagerProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvidePlatformmanagementManagerFactory.create(
                builder.platformmanagementModule));
    this.provideConfigurationManagerProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvideConfigurationManagerFactory.create(
                builder.platformmanagementModule));
    this.provideCommandControlProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvideCommandControlFactory.create(
                builder.platformmanagementModule));
    this.providePlatformCommandExecutorProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvidePlatformCommandExecutorFactory.create(
                builder.platformmanagementModule));
    this.provideCommandShellProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvideCommandShellFactory.create(
                builder.platformmanagementModule));
    this.providePlatformCommandProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvidePlatformCommandFactory.create(
                builder.platformmanagementModule));
    this.provideServiceRegistryProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvideServiceRegistryFactory.create(
                builder.platformmanagementModule));
    this.provideServiceManagerProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvideServiceManagerFactory.create(
                builder.platformmanagementModule));
    this.provideServiceCommandExecutorProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvideServiceCommandExecutorFactory.create(
                builder.platformmanagementModule));
    this.provideServiceRegistryPersistanceProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvideServiceRegistryPersistanceFactory.create(
                builder.platformmanagementModule));
    this.provideServiceDependencyPersistenceProvider =
        DoubleCheck.provider(
            PlatformmanagementModule_ProvideServiceDependencyPersistenceFactory.create(
                builder.platformmanagementModule));
  }

  @Override
  public PlatformmanagementInfo takePlatformmanagementInfo() {
    return PlatformmanagementModule_ProvidePlatformmanagementInfoFactory
        .proxyProvidePlatformmanagementInfo(platformmanagementModule);
  }

  @Override
  public PlatformmanagementManager takePlatformmanagementManager() {
    return providePlatformmanagementManagerProvider.get();
  }

  @Override
  public ConfigurationManager takeConfigurationManager() {
    return provideConfigurationManagerProvider.get();
  }

  @Override
  public CommandControl takeCommandControl() {
    return provideCommandControlProvider.get();
  }

  @Override
  public PlatformCommandExecutor takePlatformCommandExecutor() {
    return providePlatformCommandExecutorProvider.get();
  }

  @Override
  public CommandShell takeCommandShell() {
    return provideCommandShellProvider.get();
  }

  @Override
  public PlatformCommand takePlatformCommand() {
    return providePlatformCommandProvider.get();
  }

  @Override
  public ServiceRegistry takeServiceRegistry() {
    return provideServiceRegistryProvider.get();
  }

  @Override
  public ServiceManager takeServiceManager() {
    return provideServiceManagerProvider.get();
  }

  @Override
  public ServiceCommandExecutor takeServiceCommandExecutor() {
    return provideServiceCommandExecutorProvider.get();
  }

  @Override
  public ServiceRegistryPersistance takeServiceRegistryPersistance() {
    return provideServiceRegistryPersistanceProvider.get();
  }

  @Override
  public ServiceDependencyPersistence takeServiceDependencyPersistence() {
    return provideServiceDependencyPersistenceProvider.get();
  }

  public static final class Builder {
    private PlatformmanagementModule platformmanagementModule;

    private Builder() {}

    public PlatformmanagementComponent build() {
      if (platformmanagementModule == null) {
        this.platformmanagementModule = new PlatformmanagementModule();
      }
      return new DaggerPlatformmanagementComponent(this);
    }

    public Builder platformmanagementModule(PlatformmanagementModule platformmanagementModule) {
      this.platformmanagementModule = Preconditions.checkNotNull(platformmanagementModule);
      return this;
    }
  }
}

package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.command.CommandControl;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvideCommandControlFactory
    implements Factory<CommandControl> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvideCommandControlFactory(PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public CommandControl get() {
    return provideInstance(module);
  }

  public static CommandControl provideInstance(PlatformmanagementModule module) {
    return proxyProvideCommandControl(module);
  }

  public static PlatformmanagementModule_ProvideCommandControlFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvideCommandControlFactory(module);
  }

  public static CommandControl proxyProvideCommandControl(PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.provideCommandControl(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}

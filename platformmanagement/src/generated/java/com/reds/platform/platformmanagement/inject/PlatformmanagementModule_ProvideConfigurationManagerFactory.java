package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.config.ConfigurationManager;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvideConfigurationManagerFactory
    implements Factory<ConfigurationManager> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvideConfigurationManagerFactory(
      PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public ConfigurationManager get() {
    return provideInstance(module);
  }

  public static ConfigurationManager provideInstance(PlatformmanagementModule module) {
    return proxyProvideConfigurationManager(module);
  }

  public static PlatformmanagementModule_ProvideConfigurationManagerFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvideConfigurationManagerFactory(module);
  }

  public static ConfigurationManager proxyProvideConfigurationManager(
      PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.provideConfigurationManager(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}

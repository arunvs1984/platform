package com.reds.platform.platformmanagement.inject;

import com.reds.platform.redsplatformcommons.command.executor.PlatformCommandExecutor;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvidePlatformCommandExecutorFactory
    implements Factory<PlatformCommandExecutor> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvidePlatformCommandExecutorFactory(
      PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public PlatformCommandExecutor get() {
    return provideInstance(module);
  }

  public static PlatformCommandExecutor provideInstance(PlatformmanagementModule module) {
    return proxyProvidePlatformCommandExecutor(module);
  }

  public static PlatformmanagementModule_ProvidePlatformCommandExecutorFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvidePlatformCommandExecutorFactory(module);
  }

  public static PlatformCommandExecutor proxyProvidePlatformCommandExecutor(
      PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.providePlatformCommandExecutor(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}

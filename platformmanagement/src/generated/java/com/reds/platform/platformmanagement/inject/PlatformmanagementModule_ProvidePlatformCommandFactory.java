package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.command.cmd.PlatformCommand;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvidePlatformCommandFactory
    implements Factory<PlatformCommand> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvidePlatformCommandFactory(PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public PlatformCommand get() {
    return provideInstance(module);
  }

  public static PlatformCommand provideInstance(PlatformmanagementModule module) {
    return proxyProvidePlatformCommand(module);
  }

  public static PlatformmanagementModule_ProvidePlatformCommandFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvidePlatformCommandFactory(module);
  }

  public static PlatformCommand proxyProvidePlatformCommand(PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.providePlatformCommand(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}

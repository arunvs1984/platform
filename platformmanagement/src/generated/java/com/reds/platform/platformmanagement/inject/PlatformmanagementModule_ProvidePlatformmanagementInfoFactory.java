package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.info.PlatformmanagementInfo;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvidePlatformmanagementInfoFactory
    implements Factory<PlatformmanagementInfo> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvidePlatformmanagementInfoFactory(
      PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public PlatformmanagementInfo get() {
    return provideInstance(module);
  }

  public static PlatformmanagementInfo provideInstance(PlatformmanagementModule module) {
    return proxyProvidePlatformmanagementInfo(module);
  }

  public static PlatformmanagementModule_ProvidePlatformmanagementInfoFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvidePlatformmanagementInfoFactory(module);
  }

  public static PlatformmanagementInfo proxyProvidePlatformmanagementInfo(
      PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.providePlatformmanagementInfo(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}

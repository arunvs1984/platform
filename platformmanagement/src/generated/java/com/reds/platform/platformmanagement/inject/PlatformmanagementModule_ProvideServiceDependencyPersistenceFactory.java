package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.service.persistance.ServiceDependencyPersistence;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvideServiceDependencyPersistenceFactory
    implements Factory<ServiceDependencyPersistence> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvideServiceDependencyPersistenceFactory(
      PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public ServiceDependencyPersistence get() {
    return provideInstance(module);
  }

  public static ServiceDependencyPersistence provideInstance(PlatformmanagementModule module) {
    return proxyProvideServiceDependencyPersistence(module);
  }

  public static PlatformmanagementModule_ProvideServiceDependencyPersistenceFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvideServiceDependencyPersistenceFactory(module);
  }

  public static ServiceDependencyPersistence proxyProvideServiceDependencyPersistence(
      PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.provideServiceDependencyPersistence(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}

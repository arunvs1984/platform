package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.service.ServiceManager;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvideServiceManagerFactory
    implements Factory<ServiceManager> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvideServiceManagerFactory(PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public ServiceManager get() {
    return provideInstance(module);
  }

  public static ServiceManager provideInstance(PlatformmanagementModule module) {
    return proxyProvideServiceManager(module);
  }

  public static PlatformmanagementModule_ProvideServiceManagerFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvideServiceManagerFactory(module);
  }

  public static ServiceManager proxyProvideServiceManager(PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.provideServiceManager(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}

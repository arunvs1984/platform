package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.service.ServiceRegistry;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvideServiceRegistryFactory
    implements Factory<ServiceRegistry> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvideServiceRegistryFactory(PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public ServiceRegistry get() {
    return provideInstance(module);
  }

  public static ServiceRegistry provideInstance(PlatformmanagementModule module) {
    return proxyProvideServiceRegistry(module);
  }

  public static PlatformmanagementModule_ProvideServiceRegistryFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvideServiceRegistryFactory(module);
  }

  public static ServiceRegistry proxyProvideServiceRegistry(PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.provideServiceRegistry(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}

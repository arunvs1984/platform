package com.reds.platform.platformmanagement.inject;

import com.reds.platform.platformmanagement.service.persistance.ServiceRegistryPersistance;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlatformmanagementModule_ProvideServiceRegistryPersistanceFactory
    implements Factory<ServiceRegistryPersistance> {
  private final PlatformmanagementModule module;

  public PlatformmanagementModule_ProvideServiceRegistryPersistanceFactory(
      PlatformmanagementModule module) {
    this.module = module;
  }

  @Override
  public ServiceRegistryPersistance get() {
    return provideInstance(module);
  }

  public static ServiceRegistryPersistance provideInstance(PlatformmanagementModule module) {
    return proxyProvideServiceRegistryPersistance(module);
  }

  public static PlatformmanagementModule_ProvideServiceRegistryPersistanceFactory create(
      PlatformmanagementModule module) {
    return new PlatformmanagementModule_ProvideServiceRegistryPersistanceFactory(module);
  }

  public static ServiceRegistryPersistance proxyProvideServiceRegistryPersistance(
      PlatformmanagementModule instance) {
    return Preconditions.checkNotNull(
        instance.provideServiceRegistryPersistance(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}

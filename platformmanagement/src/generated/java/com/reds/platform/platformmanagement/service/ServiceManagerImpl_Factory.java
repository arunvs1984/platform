package com.reds.platform.platformmanagement.service;

import dagger.internal.Factory;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ServiceManagerImpl_Factory implements Factory<ServiceManagerImpl> {
  private static final ServiceManagerImpl_Factory INSTANCE = new ServiceManagerImpl_Factory();

  @Override
  public ServiceManagerImpl get() {
    return provideInstance();
  }

  public static ServiceManagerImpl provideInstance() {
    return new ServiceManagerImpl();
  }

  public static ServiceManagerImpl_Factory create() {
    return INSTANCE;
  }

  public static ServiceManagerImpl newServiceManagerImpl() {
    return new ServiceManagerImpl();
  }
}

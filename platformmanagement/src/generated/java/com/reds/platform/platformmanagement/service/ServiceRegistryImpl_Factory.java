package com.reds.platform.platformmanagement.service;

import dagger.internal.Factory;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ServiceRegistryImpl_Factory implements Factory<ServiceRegistryImpl> {
  private static final ServiceRegistryImpl_Factory INSTANCE = new ServiceRegistryImpl_Factory();

  @Override
  public ServiceRegistryImpl get() {
    return provideInstance();
  }

  public static ServiceRegistryImpl provideInstance() {
    return new ServiceRegistryImpl();
  }

  public static ServiceRegistryImpl_Factory create() {
    return INSTANCE;
  }

  public static ServiceRegistryImpl newServiceRegistryImpl() {
    return new ServiceRegistryImpl();
  }
}

package com.reds.platform.platformmanagement.service.persistance;

import dagger.internal.Factory;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ServiceRegistryPersistanceImpl_Factory
    implements Factory<ServiceRegistryPersistanceImpl> {
  private static final ServiceRegistryPersistanceImpl_Factory INSTANCE =
      new ServiceRegistryPersistanceImpl_Factory();

  @Override
  public ServiceRegistryPersistanceImpl get() {
    return provideInstance();
  }

  public static ServiceRegistryPersistanceImpl provideInstance() {
    return new ServiceRegistryPersistanceImpl();
  }

  public static ServiceRegistryPersistanceImpl_Factory create() {
    return INSTANCE;
  }

  public static ServiceRegistryPersistanceImpl newServiceRegistryPersistanceImpl() {
    return new ServiceRegistryPersistanceImpl();
  }
}

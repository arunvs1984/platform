package com.reds.platform.platformmanagement.service.persistance;

import dagger.internal.Factory;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ServieDependencyPersistenceImpl_Factory
    implements Factory<ServieDependencyPersistenceImpl> {
  private static final ServieDependencyPersistenceImpl_Factory INSTANCE =
      new ServieDependencyPersistenceImpl_Factory();

  @Override
  public ServieDependencyPersistenceImpl get() {
    return provideInstance();
  }

  public static ServieDependencyPersistenceImpl provideInstance() {
    return new ServieDependencyPersistenceImpl();
  }

  public static ServieDependencyPersistenceImpl_Factory create() {
    return INSTANCE;
  }

  public static ServieDependencyPersistenceImpl newServieDependencyPersistenceImpl() {
    return new ServieDependencyPersistenceImpl();
  }
}

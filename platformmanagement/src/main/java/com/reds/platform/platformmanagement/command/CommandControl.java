package com.reds.platform.platformmanagement.command;

import com.reds.platform.redsplatformcommons.exception.PlatformCommandException;

public interface CommandControl {

	public void start() throws PlatformCommandException;

	public void stop() throws PlatformCommandException;

}

package com.reds.platform.platformmanagement.command;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.reds.platform.platformmanagement.inject.PlatformmanagementTreasury;
import com.reds.platform.platformmanagement.tracker.PlatformmanagementTrack;
import com.reds.platform.redsplatformcommons.command.executor.PlatformCommandExecutor;
import com.reds.platform.redsplatformcommons.command.executor.ServiceCommandExecutor;
import com.reds.platform.redsplatformcommons.exception.PlatformCommandException;

@Singleton
public class CommandControlImpl implements CommandControl {
	/**
	 * 
	 */

	private CommandShell commandShell = null;

	@Inject
	public CommandControlImpl() {

		this.commandShell = PlatformmanagementTreasury.open.takeCommandShell();

	}

	@Override
	public void start() throws PlatformCommandException {
		PlatformmanagementTrack.me.info("Starting Platform Command Control");
		this.commandShell.start();
		PlatformmanagementTrack.me.info("Platform Command Control S");
	}

	@Override
	public void stop() throws PlatformCommandException {
		// TODO Auto-generated method stub

	}

	public final CommandShell getCommandShell() {
		return commandShell;
	}

}

package com.reds.platform.platformmanagement.command;

import java.security.MessageDigest;
import java.util.concurrent.Callable;

import picocli.CommandLine.Option;

public class Login implements Callable<Object> {
	@Option(names = { "-u", "--user" }, description = "User name")
	String user;

	@Option(names = { "-p", "--password" }, description = "Passphrase", interactive = true)
	String password;

	public Object call() throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(password.getBytes());
		System.out.printf("Hi %s, your passphrase is hashed to %s.%n", user, base64(md.digest()));
		return null;
	}

	private String base64(byte[] arr) {
		return "99999999999999";
		/* ... */ }

	@Override
	public String toString() {
		return "Login [user=" + user + ", password=" + password + "]";
	}

}
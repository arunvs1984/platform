package com.reds.platform.platformmanagement.command.cmd;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.budhash.cliche.Command;
import com.reds.platform.platformmanagement.command.Login;
import com.reds.platform.platformmanagement.inject.PlatformmanagementTreasury;
import com.reds.platform.platformmanagement.tracker.PlatformmanagementTrack;
import com.reds.platform.redsplatformcommons.command.CommandResponse;
import com.reds.platform.redsplatformcommons.command.DeployCmd;
import com.reds.platform.redsplatformcommons.command.InfoCmd;
import com.reds.platform.redsplatformcommons.command.ListCmd;
import com.reds.platform.redsplatformcommons.command.LogsPathCmd;
import com.reds.platform.redsplatformcommons.command.StartCmd;
import com.reds.platform.redsplatformcommons.command.StopCmd;
import com.reds.platform.redsplatformcommons.command.SystemInfoCmd;
import com.reds.platform.redsplatformcommons.command.UnDeployCmd;
import com.reds.platform.redsplatformcommons.command.executor.PlatformCommandExecutor;
import com.reds.platform.redsplatformcommons.command.executor.ServiceCommandExecutor;
import com.reds.platform.redsplatformcommons.exception.PlatformCommandException;
import com.reds.platform.redsplatformcommons.inject.RedsplatformcommonsTreasury;

import picocli.CommandLine;

@Singleton
public class PlatformCommand {

	private PlatformCommandExecutor platormCommandExecutor;
	private ServiceCommandExecutor serviceCommandExecutor;

	@Inject
	public PlatformCommand() {
		this.platormCommandExecutor = PlatformmanagementTreasury.open.takePlatformmanagementManager();
		this.serviceCommandExecutor = PlatformmanagementTreasury.open.takeServiceCommandExecutor();
	}
	
	
	
	@Command(name = "logPath", abbrev = "logPath", description = "To get  logPath platform")
	public void logPathPlatform() {
		try {

			LogsPathCmd cmd = new LogsPathCmd();
			printResponse(serviceCommandExecutor.logPath(cmd));
		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to get logPathPlatform ", e);
		}
	}

	@Command(name = "logPath", abbrev = "logPath", description = "To get  logPath of service")
	public void logPathService(String nParam, String name, String vParam, String version) {
		try {

			LogsPathCmd cmd = new LogsPathCmd();
			cmd.setServiceName(name);
			cmd.setServiceVersion(vParam);
			printResponse(serviceCommandExecutor.logPath(cmd));
		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to get logPathService {}:{}", name, version, e);
		}
	}

	@Command(name = "systeminfo", abbrev = "systeminfo", description = "To get System Info")
	public void systemInfo() {
		try {
			printResponse(this.platormCommandExecutor.systemInfo(new SystemInfoCmd()));

		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to get systemInfo  ", e);
		}

	}

	@Command(name = "gc", abbrev = "gc", description = "To initate garbage collection")
	public void gc() {
		try {
			printResponse(this.platormCommandExecutor.runGc());

		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to run  gc ", e);
		}

	}

	@Command(name = "info", abbrev = "info", description = "To display platform information")
	public void info() {
		try {
			printResponse(this.platormCommandExecutor.info(new InfoCmd()));

		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to get  info ", e);
		}

	}

	private void printResponse(CommandResponse response) {
		System.out.println(response);

	}

	@Command(name = "login", abbrev = "login", description = "To login ")
	public void login(String userName) {
		Login login = new Login();
		// Login login=CommandLine.populateCommand(new Login(), args);
		CommandLine.call(login, "-u", userName, "-p");
		System.out.println(login);
	}

	/**
	 * deploy -l zipFileName.zip This file name is expected to be inside market
	 * place
	 * 
	 * @param param1
	 * @param location
	 */
	@Command(name = "deploy", abbrev = "deploy", description = "To deploy service")
	public void deploy(String param1, String location) {
		try {
			location = RedsplatformcommonsTreasury.open.takePlatformLayout().getMarketPlace() + location;
			PlatformmanagementTrack.me.info("Deploying Service {} ", location);
			DeployCmd deployCmd = new DeployCmd();
			deployCmd.setLocation(location);
			printResponse(serviceCommandExecutor.deploy(deployCmd));
		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to deploy service " + location, e);
		}
	}

	@Command(name = "undeploy", abbrev = "undeploy", description = "To undeploy service")
	public void undeploy(String nParam, String name, String vParam, String version) {
		try {
			PlatformmanagementTrack.me.info("UnDeploying Service {}:{} ", name, version);
			UnDeployCmd cmd = new UnDeployCmd();
			cmd.setName(name);
			cmd.setVersion(version);
			printResponse(serviceCommandExecutor.UnDeploy(cmd));
		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to undeploy service " + name + ":" + version, e);
		}
	}

	@Command(name = "start", abbrev = "start", description = "To start service")
	public void startService(String nParam, String name, String vParam, String version) {
		try {
			PlatformmanagementTrack.me.info("Starting Service {}:{} ", name, version);
			StartCmd cmd = new StartCmd();
			cmd.setName(name);
			cmd.setVersion(version);
			printResponse(serviceCommandExecutor.startService(cmd));
		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to start service " + name + ":" + version, e);
		}
	}

	@Command(name = "stop", abbrev = "stop", description = "To stop service", header = "Service Stopped")
	public void stopService(String nParam, String name, String vParam, String version) {
		try {
			PlatformmanagementTrack.me.info("Stopping Service {}:{} ", name, version);
			StopCmd cmd = new StopCmd();
			cmd.setName(name);
			cmd.setVersion(version);
			printResponse(serviceCommandExecutor.stopService(cmd));
		} catch (Exception e) {
			PlatformmanagementTrack.me.error("Failed to stop service " + name + ":" + version, e);
		}
	}

	/**
	 * <b>Usage:</b> list m for market place deployed services <br>
	 * list p for platform deployed services
	 * 
	 * @param type
	 */
	@Command(name = "list", abbrev = "list", description = "To list services or node or market place services", header = "Listing Deployed Services on Platform or Market Place")
	public void listService(String type) {
		try {
			ListCmd listCmd = new ListCmd();
			listCmd.setType(type);
			if (type == null || type.equals("p")) {
				printResponse(this.serviceCommandExecutor.listPlatformDeployedServices(listCmd));
			} else {
				printResponse(this.serviceCommandExecutor.listMarketPlaceServices(listCmd));
			}
		} catch (PlatformCommandException e) {
			PlatformmanagementTrack.me.error("Failed to list service ", e);
		}

	}

	public PlatformCommandExecutor getExecutor() {
		return platormCommandExecutor;
	}

	public void setExecutor(PlatformCommandExecutor executor) {
		this.platormCommandExecutor = executor;
	}

}

package com.reds.platform.platformmanagement.config;

/**
 * <b>Purpose:</b> This class hold all the configuration related to
 * communication channel.
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 * 
 */
public class CommunicationChannelConfig {

	/**
	 * To hold name of Messaging Channel
	 */
	private String channelName = "redschannel";
	/**
	 * To hold port number of Messaging Channel
	 */
	private int channelPort = 9999;

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public int getChannelPort() {
		return channelPort;
	}

	public void setChannelPort(int channelPort) {
		this.channelPort = channelPort;
	}

}
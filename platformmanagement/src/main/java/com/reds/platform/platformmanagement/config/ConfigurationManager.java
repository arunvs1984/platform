package com.reds.platform.platformmanagement.config;

import javax.inject.Singleton;

import com.reds.library.redscommons.exception.RedscommonsException;
import com.reds.library.redscommons.tracker.TrackerConfig;
import com.reds.library.redsutils.RedsDateUtils;
import com.reds.platform.redsplatformcommons.config.AbstractConfigurationManager;
import com.reds.platform.redsplatformcommons.inject.RedsplatformcommonsTreasury;

@Singleton
public class ConfigurationManager extends AbstractConfigurationManager {

	private PlatformmanagementConfig platformmanagementConfig = null;

	private TrackerConfig trackerConfig = null;

	private PlatformInfo platformInfo = null;

	public ConfigurationManager() throws RedscommonsException {
		super(RedsplatformcommonsTreasury.open.takePlatformLayout().getPlatformConfig());
		initPlatformConfig();
		initTrackerConfig();
		initPlatformInfo();
	}

	private void initPlatformInfo() throws RedscommonsException {
		PlatformInfo info = new PlatformInfo();
		info.setReleaseDate(RedsDateUtils.getCurrenTimeStamp());
		this.platformInfo = (PlatformInfo) initConfig(info);

	}

	private void initTrackerConfig() throws RedscommonsException {
		this.trackerConfig = (TrackerConfig) initConfig(new TrackerConfig());

	}

	private void initPlatformConfig() throws RedscommonsException {
		this.platformmanagementConfig = (PlatformmanagementConfig) initConfig(new PlatformmanagementConfig());
	}

	public PlatformmanagementConfig getPlatformmanagementConfig() {
		return platformmanagementConfig;
	}

	public void setPlatformmanagementConfig(PlatformmanagementConfig platformmanagementConfig) {
		this.platformmanagementConfig = platformmanagementConfig;
	}

	public TrackerConfig getTrackerConfig() {
		return trackerConfig;
	}

	public void setTrackerConfig(TrackerConfig trackerConfig) {
		this.trackerConfig = trackerConfig;
	}

	public PlatformInfo getPlatformInfo() {
		return platformInfo;
	}

	public void setPlatformInfo(PlatformInfo platformInfo) {
		this.platformInfo = platformInfo;
	}

}

package com.reds.platform.platformmanagement.config;

import com.reds.library.redscommons.config.PlatformConfig;

public class PlatformInfo implements PlatformConfig {
	private String id = "PlatformInfo";
	private String name = "Reds Platform";
	private String version = "1.0.0";
	private String releaseDate;

	public PlatformInfo() {
	}

	@Override
	public String getId() {

		return id;
	}

	@Override
	public String getDescription() {

		return "";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder= new StringBuilder();
		stringBuilder.append(         name           )
		.append("\n").append("Version  :").append(version)
		.append("\n").append("Released :").append(releaseDate);
		return stringBuilder.toString();
	}

}

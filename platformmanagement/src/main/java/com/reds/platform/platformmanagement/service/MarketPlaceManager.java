package com.reds.platform.platformmanagement.service;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.platform.platformmanagement.exception.ServiceManagementException;
import com.reds.platform.platformmanagement.inject.PlatformmanagementTreasury;
import com.reds.platform.platformmanagement.tracker.PlatformmanagementTrack;
import com.reds.platform.redsplatformcommons.inject.RedsplatformcommonsTreasury;
import com.reds.platform.redsplatformcommons.service.ServiceInformation;
import com.reds.platform.servicecommons.exception.ServiceException;
import com.reds.platform.servicecommons.exception.ServicecommonsErrorCodes;

public class MarketPlaceManager {

	public List<ServiceInformation> getAllServiceInfo() throws ServiceException {
		String marketPlacePath = RedsplatformcommonsTreasury.open.takePlatformLayout().getMarketPlace();
		List<ServiceInformation> serviceInformations = new ArrayList<>();
		try {
			List<Path> allServices = RedsFileUtils.getAllFiles(marketPlacePath);
			for (Path zipLocation : allServices) {
				try {
					if(!zipLocation.toString().endsWith(".zip")) {
						continue;
					}
					ServiceInformation serviceInformation = PlatformmanagementTreasury.open.takeServiceManager()
							.getServiceInformation(zipLocation.toString(),true);

					ServiceRegistryEntry entry = PlatformmanagementTreasury.open.takeServiceRegistry().getServiceEntry(
							serviceInformation.getMetaData().getName(), serviceInformation.getMetaData().getVersion());
					if (entry != null) {
						serviceInformation.setStatus(entry.getServiceInfo().getStatus());
					}

					serviceInformations.add(serviceInformation);
				} catch (ServiceManagementException e) {
					PlatformmanagementTrack.me.error("Failed to get Service Information of Service at Path {} ",
							zipLocation);
				}
			}

		} catch (RedsutilsException e) {
			throw new ServiceException(ServicecommonsErrorCodes.errorCode(1),
					"Failed to get all services from market place " + marketPlacePath, e);
		}
		return serviceInformations;
	}

}

package com.reds.platform.platformmanagement.service;

import com.reds.platform.platformmanagement.exception.ServiceManagementException;
import com.reds.platform.redsplatformcommons.command.executor.ServiceCommandExecutor;
import com.reds.platform.redsplatformcommons.service.ServiceInformation;

public interface ServiceManager extends ServiceCommandExecutor {

	public void start() throws ServiceManagementException;

	public void stop() throws ServiceManagementException;

	public ServiceInformation getServiceInformation(String zipLocation,boolean marketPlace) throws ServiceManagementException;

	public void setRegistry(ServiceRegistry serviceRegistry);

	

}

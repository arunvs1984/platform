package com.reds.platform.platformmanagement.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.reds.library.redscommons.exception.RedsXmlException;
import com.reds.library.redscommons.xml.RedsXmlManager;
import com.reds.library.redscommons.xml.RedsXmlManagerImpl;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.platform.platformmanagement.PlatformManagementUtils;
import com.reds.platform.platformmanagement.exception.PlatformmanagementErrorCodes;
import com.reds.platform.platformmanagement.exception.ServiceManagementException;
import com.reds.platform.platformmanagement.inject.PlatformmanagementTreasury;
import com.reds.platform.platformmanagement.service.persistance.ServiceDependencyPersistanceObject;
import com.reds.platform.platformmanagement.service.persistance.ServiceInfoPersistanceObject;
import com.reds.platform.platformmanagement.service.persistance.ServiceRegistryPersistance;
import com.reds.platform.platformmanagement.tracker.PlatformmanagementTrack;
import com.reds.platform.redsplatformcommons.command.CommandResponse;
import com.reds.platform.redsplatformcommons.command.DeployCmd;
import com.reds.platform.redsplatformcommons.command.ListCmd;
import com.reds.platform.redsplatformcommons.command.LogsPathCmd;
import com.reds.platform.redsplatformcommons.command.StartCmd;
import com.reds.platform.redsplatformcommons.command.StopCmd;
import com.reds.platform.redsplatformcommons.command.UnDeployCmd;
import com.reds.platform.redsplatformcommons.exception.PlatformCommandException;
import com.reds.platform.redsplatformcommons.inject.RedsplatformcommonsTreasury;
import com.reds.platform.redsplatformcommons.layout.PlatformLayout;
import com.reds.platform.redsplatformcommons.service.ServiceInformation;
import com.reds.platform.redsplatformcommons.service.ServiceMetaData;
import com.reds.platform.redsplatformcommons.service.ServiceStatus;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.platform.servicecommons.Service;
import com.reds.platform.servicecommons.ServiceLookUp;
import com.reds.platform.servicecommons.context.CloseContext;
import com.reds.platform.servicecommons.context.DeploymentContext;
import com.reds.platform.servicecommons.context.InitContext;
import com.reds.platform.servicecommons.context.ServiceContext;
import com.reds.platform.servicecommons.exception.ServiceException;

@Singleton
public class ServiceManagerImpl implements ServiceManager {

	private ServiceRegistry serviceRegistry = null;
	private MarketPlaceManager marketPlaceManager = null;

	@Inject
	public ServiceManagerImpl() {
		this.marketPlaceManager = new MarketPlaceManager();
		this.serviceRegistry = PlatformmanagementTreasury.open.takeServiceRegistry();
	}

	@Override
	public void setRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}

	@Override
	public void start() throws ServiceManagementException {
		try {
			PlatformmanagementTrack.me.info("Starting Platform Service Management");

			this.serviceRegistry.load();
			startloadedServices();
			PlatformmanagementTrack.me.info("Platform Service Management Started");
		} catch (Exception e) {
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to start service management", e);
		}
	}

	@Override
	public void stop() throws ServiceManagementException {
		if (this.serviceRegistry != null) {
			this.serviceRegistry.stop();
		}

	}

	@SuppressWarnings("rawtypes")
	@Override
	public CommandResponse deploy(DeployCmd deployCmd) throws PlatformCommandException {
		try {
			CommandResponse<ServiceMetaData> response = new CommandResponse<>();
			ServiceMetaData metaData = this.isAlreadyDeployed(deployCmd);
			if (metaData != null) {
				response.setMessage("Service " + metaData.getName() + ":" + metaData.getVersion()
						+ " Already Deployed. Please Undeploy first");
				return response;
			}
			ServiceInformation information = this.deployService(deployCmd);
			metaData = information.getMetaData();

			/* Setting Service Status */
			setStatus(ServiceStatus.DEPLOYED, metaData.getName(), metaData.getVersion());
			response.setMessage("Service Deployed " + metaData.getName() + ":" + metaData.getVersion());
			response.setResponse(metaData);
			return response;
		} catch (ServiceManagementException e) {
			throw new PlatformCommandException(PlatformmanagementErrorCodes.errorCode(1), "Failed to deploy service",
					e);
		}

	}

	private void setStatus(ServiceStatus status, String name, String version) throws ServiceManagementException {
		this.serviceRegistry.setStatus(status, name, version);

	}

	private ServiceMetaData isAlreadyDeployed(DeployCmd deployCmd) throws ServiceManagementException {
		PlatformLayout layout = RedsplatformcommonsTreasury.open.takePlatformLayout();
		String serviceTempRoot = null;
		try {
			serviceTempRoot = getTempRootPath(layout, "tmp", deployCmd.getLocation());
			/* Read Meta Data File */
			ServiceMetaData serviceMetaData = getServiceMetaData(serviceTempRoot);
			Service service = this.serviceRegistry.getService(serviceMetaData.getName(), serviceMetaData.getVersion());
			if (service == null) {
				return null;
			} else {
				return serviceMetaData;
			}

		} catch (RedsutilsException | RedsXmlException | ServiceException e) {
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1), "Failed to deploy services",
					e);
		} finally {
			deleteTempFile(serviceTempRoot);
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public CommandResponse stopService(StopCmd cmd) throws PlatformCommandException {
		String name = cmd.getName();
		String version = cmd.getVersion();

		try {
			ServiceRegistryEntry serviceRegistryEntry = this.serviceRegistry.getServiceEntry(name, version);
			Service service = serviceRegistryEntry.getService();
			CommandResponse commandResponse = new CommandResponse<>();
			if (service == null) {
				commandResponse.setMessage("Service " + name + ":" + version + " not found");
				return commandResponse;

			}
			ServiceInformation serviceInformation = new ServiceInformation();
			/* TODO need to add required elements to CloseContext */
			CloseContext initContext = new CloseContext();
			try {
				service.close(initContext);
				/* Setting Service Status to CLOSED */
				setStatus(ServiceStatus.CLOSED, name, version);
			} catch (Exception e) {
				PlatformmanagementTrack.me.error("Service Exception While Closing Service {} :{} ", name, version, e);
				commandResponse.setMessage(
						"Service exception " + e.getMessage() + "while closing Service " + name + ":" + version + "  ");
			}
			/* TODO need to add required elements to RedsContext */
			ServiceContext context = new ServiceContext((ServiceLookUp) this.serviceRegistry,
					serviceInformation.getServiceConfigLocation());
			try {
				service.stop(context);
				/* Setting Service Status to CLOSED */
				setStatus(ServiceStatus.STOPPED, name, version);
				commandResponse.setMessage("Service " + name + ":" + version + " stoped ");
			} catch (Exception e) {
				PlatformmanagementTrack.me.error("Service Exception While Stopping Service {} :{} ", name, version, e);
				commandResponse.setMessage("Service exception " + e.getMessage() + "while Stopping Service " + name
						+ ":" + version + "  ");
			}
			return commandResponse;
		} catch (ServiceManagementException e) {
			throw new PlatformCommandException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to stop services " + name + ":" + version, e);
		}

	}

	@SuppressWarnings("rawtypes")
	@Override
	public CommandResponse startService(StartCmd cmd) throws PlatformCommandException {
		String name = cmd.getName();
		String version = cmd.getVersion();

		try {
			ServiceRegistryEntry serviceRegistryEntry = this.serviceRegistry.getServiceEntry(name, version);
			Service service = serviceRegistryEntry.getService();
			CommandResponse commandResponse = new CommandResponse<>();
			if (service == null) {
				commandResponse.setMessage("Service " + name + ":" + version + " not found");
				return commandResponse;

			}
			ServiceInformation serviceInformation = serviceRegistryEntry.getServiceInfo();
			/* TODO need to add required elements to InitContext */
			InitContext initContext = new InitContext();
			try {
				service.init(initContext);
				/* Setting Service Status to INITIALIZED */
				setStatus(ServiceStatus.INITIALIZED, name, version);
			} catch (Exception e) {
				PlatformmanagementTrack.me.error("Service Exception While Initializing Service {} :{} ", name, version,
						e);
				commandResponse.setMessage("Service exception " + e.getMessage() + "while initializing Service " + name
						+ ":" + version + "  ");
			}
			/* TODO need to add required elements to RedsContext */
			ServiceContext context = new ServiceContext((ServiceLookUp) this.serviceRegistry,
					serviceInformation.getServiceConfigLocation());
			context.setServiceName(name);
			context.setServiceVersion(version);
			try {
				service.start(context);
				//TODO Need to relook
				/* Setting Service Status to STARTED */
				setStatus(ServiceStatus.STARTED, name, version);
				commandResponse.setMessage("Service " + name + ":" + version + " started ");
			} catch (Exception e) {
				this.serviceRegistry.printLoadedClass();
				PlatformmanagementTrack.me.error("Service Exception While Starting Service {} :{} ", name, version, e);
				commandResponse.setMessage("Service exception " + e.getMessage() + "while starting Service " + name
						+ ":" + version + "  ");
			}
			return commandResponse;
		} catch (ServiceManagementException e) {
			throw new PlatformCommandException(PlatformmanagementErrorCodes.errorCode(1), "Failed to start services",
					e);
		}

	}

	private ServiceInformation deployService(DeployCmd deployCmd) throws ServiceManagementException {

		try {
			ServiceInformation serviceInformation = getServiceInformation(deployCmd.getLocation(), false);
			if (serviceInformation.getMetaData().isManagedLib()) {

				/* Managing dependencies of this service */
				manageDependencies(serviceInformation);
				/* Expecting restart for managed lib service */
				this.serviceRegistry.register(serviceInformation, false);
				PlatformmanagementTrack.me.warn("Platform Managed Lib Service Deployed. PLATFORM RESTART REQUIRED");
			} else {

				/* Expecting restart for managed lib service */
				this.serviceRegistry.register(serviceInformation, true);
			}

			return serviceInformation;
		} catch (ServiceException e) {
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1), "Failed to deploy services",
					e);
		}

	}

	@Override
	public ServiceInformation getServiceInformation(String zipLocation, boolean marketPlace)
			throws ServiceManagementException {
		PlatformLayout layout = RedsplatformcommonsTreasury.open.takePlatformLayout();
		String serviceTempRoot = getTempRootPath(layout, "temp", zipLocation);

		try {

			/* Read Meta Data File */
			ServiceMetaData serviceMetaData = getServiceMetaData(serviceTempRoot);

			String serviceFolder = null;
			String serviceConfigLocation = null;
			String serviceLibs = null;
			if (serviceMetaData.getType() == ServiceType.BUSINESS) {
				/* Copy Business Service to Business Pot */
				String businessPot = layout.getBusinessServiceRoot();
				serviceFolder = businessPot + serviceMetaData.getName() + serviceMetaData.getVersion() + "/";
				serviceConfigLocation = serviceFolder + "/" + layout.getBusinessServiceConfig();
				serviceLibs = serviceFolder + "/" + layout.getBusinessServiceLibs();

			} else {
				/* Copy Platform Service to Reds Pot */
				String redsPot = layout.getPlatformServiceRoot();
				serviceFolder = redsPot + serviceMetaData.getName() + serviceMetaData.getVersion() + "/";
				serviceConfigLocation = serviceFolder + "/" + layout.getPlatformServiceConfig();
				serviceLibs = serviceFolder + "/" + layout.getPlatformServiceLibs();

			}
			ServiceInformation serviceInformation = new ServiceInformation();
			if (!marketPlace) {
				RedsFileUtils.makeDirForcefuly(serviceFolder);
				RedsFileUtils.copyDir(serviceTempRoot, serviceFolder);
			}

			/* Copy service icon */
			String icon = serviceMetaData.getIcon();
			if (icon != null) {
				icon = serviceTempRoot + "/" + icon;
				String destination = layout.getMarketPlaceIcon() + "/" + serviceMetaData.getName()
						+ serviceMetaData.getVersion() + ".jpg";
				RedsFileUtils.copy(new File(icon), new File(destination));
				serviceInformation.setServiceIcon(destination);
			}

			serviceInformation.setMetaData(serviceMetaData);
			serviceInformation.setServiceLocation(serviceFolder);
			serviceInformation.setServiceConfigLocation(serviceConfigLocation);
			serviceInformation.setServiceLibs(serviceLibs);
			return serviceInformation;
		} catch (ServiceManagementException | RedsutilsException | RedsXmlException | IOException e) {
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1), "Failed to deploy services",
					e);
		} finally {
			deleteTempFile(serviceTempRoot);
		}

	}

	/**
	 * <b>Step 1</b> Copy dependencies to Platform Lib folder <br>
	 * <b>Step 2</b> Update Service Dependency Persistence Object
	 * 
	 * @param serviceInformation
	 * @throws ServiceManagementException
	 */
	private void manageDependencies(ServiceInformation serviceInformation) throws ServiceManagementException {

		String serviceLibFolder = serviceInformation.getServiceLibs();
		PlatformLayout layout = RedsplatformcommonsTreasury.open.takePlatformLayout();
		String platformLibs = layout.getPlatformLib();

		try {
			List<Path> allServiceLibs = RedsFileUtils.getAllFiles(serviceLibFolder);
			ServiceDependencyPersistanceObject serviceDependencyPersistanceObject = PlatformmanagementTreasury.open
					.takeServiceDependencyPersistence().getServiceDependencyPersistanceObject();
			String serviceKey = PlatformManagementUtils.getServiceKey(serviceInformation.getMetaData().getName(),
					serviceInformation.getMetaData().getVersion());
			for (Path libraryName : allServiceLibs) {
				serviceDependencyPersistanceObject.addDependency(libraryName.getFileName().toString(), serviceKey);
			}

			PlatformmanagementTreasury.open.takeServiceDependencyPersistence().save(serviceDependencyPersistanceObject);

			RedsFileUtils.moveFiles(serviceLibFolder, platformLibs);
			RedsFileUtils.delete(serviceLibFolder);

		} catch (RedsutilsException e) {
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to copy dependencies from service lib folder to platform lib folder");
		}

	}

	private void deleteTempFile(String serviceTempRoot) {
		if (serviceTempRoot != null) {
			try {
				Path rootPath = Paths.get(serviceTempRoot);
				Files.walk(rootPath).sorted(Comparator.reverseOrder()).map(Path::toFile).peek(e -> {
					PlatformmanagementTrack.me.debug("Deleting {}", e);
				}).forEach(File::delete);

				// String newName = serviceTempRoot + "_" + RedsDateUtils.getNow().getTime();
				// RedsFileUtils.rename(serviceTempRoot, newName);
				// Files.deleteIfExists(new File(serviceTempRoot).toPath());
				// RedsFileUtils.forceDelete(serviceTempRoot);
			} catch (IOException e) {
				// TODO Auto-generated catch blocks
				e.printStackTrace();
			}
		}
	}

	private ServiceMetaData getServiceMetaData(String serviceTempRoot)
			throws RedsutilsException, RedsXmlException, ServiceManagementException {
		RedsXmlManager<ServiceMetaData> xmlManager = new RedsXmlManagerImpl<>(serviceTempRoot);
		ServiceMetaData serviceMetaData = xmlManager.read(new ServiceMetaData(), ServiceMetaData.class);
		return serviceMetaData;
	}

	private String getTempRootPath(PlatformLayout layout, String folderName, String zipLocation)
			throws ServiceManagementException {

		String workArea = layout.getWorkArea();
		String temp = workArea + "/" + folderName + "/" + "service" + System.currentTimeMillis()
				+ "/";
		String serviceTempRoot = null;

		/* Creating Temp Folder */
		try {
			RedsFileUtils.makeDirForcefuly(temp);
			RedsFileUtils.unZip(zipLocation, temp);
			String hanger = layout.getHangar();
			/* Intimate maintenance process */
			RedsFileUtils.write(hanger + "tempremovables.txt", "." + workArea + "/" + "tmp", false);
			Path metaDataFileLocation = RedsFileUtils.scanAndGetFilePath(temp, layout.getServicMetaDataName() + ".xml");
			if (metaDataFileLocation == null) {
				throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1),
						"Service deployment failed. Service Meta Data Not found ");
			}
			serviceTempRoot = metaDataFileLocation.toFile().getParent();
		} catch (Exception e) {
			deleteTempFile(temp);
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to get TempRootPath for deploying  " + zipLocation, e);

		}
		return serviceTempRoot;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public CommandResponse listPlatformDeployedServices(ListCmd cmd) throws PlatformCommandException {
		try {
			List<ServiceInformation> serviceInformations = this.serviceRegistry.getAllServiceInfo();
			CommandResponse<List<ServiceInformation>> commandResponse = new CommandResponse<>();
			commandResponse.setMessage(serviceInformations.size() + " Services Found ");
			commandResponse.setResponse(serviceInformations);
			return commandResponse;
		} catch (ServiceException e) {
			throw new PlatformCommandException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to list deployed services", e);
		}

	}

	@SuppressWarnings("rawtypes")
	@Override
	public CommandResponse listMarketPlaceServices(ListCmd cmd) throws PlatformCommandException {
		try {
			List<ServiceInformation> serviceInformations = this.marketPlaceManager.getAllServiceInfo();
			CommandResponse<List<ServiceInformation>> commandResponse = new CommandResponse<>();
			commandResponse.setMessage(serviceInformations.size() + " Services Found ");
			commandResponse.setResponse(serviceInformations);
			return commandResponse;
		} catch (ServiceException e) {
			throw new PlatformCommandException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to list deployed services", e);
		}

	}

	@SuppressWarnings("rawtypes")
	@Override
	public CommandResponse UnDeploy(UnDeployCmd unDeployCmd) throws PlatformCommandException {
		try {
			String name = unDeployCmd.getName();
			String version = unDeployCmd.getVersion();
			CommandResponse commandResponse = new CommandResponse<>();
			Service service = this.serviceRegistry.getService(name, version);
			if (service == null) {
				commandResponse.setMessage("Service " + name + ":" + version + " not found");
				return commandResponse;

			}
			/* Setting Service Status to unDeployed */
			setStatus(ServiceStatus.UNDEPLOYED, name, version);

			ServiceInformation information = this.serviceRegistry.unRegister(name, version);

			if (information.getMetaData().isManagedLib()) {
				/* Remove Dependencies */
				ServiceDependencyPersistanceObject dependencyPersistanceObject = PlatformmanagementTreasury.open
						.takeServiceDependencyPersistence().getServiceDependencyPersistanceObject();
				List<String> libsToRemove = dependencyPersistanceObject.removeDependencyOfService(
						PlatformManagementUtils.getServiceKey(information.getMetaData().getName(),
								information.getMetaData().getVersion()));
				PlatformLayout layout = RedsplatformcommonsTreasury.open.takePlatformLayout();
				String hangar = layout.getHangar();
				StringBuilder removables = new StringBuilder();
				for (String lib : libsToRemove) {
					removables.append("/" + "lib" + "/" + lib).append(",");
				}
				RedsFileUtils.write(hangar + "removables.txt", removables.toString(), true);
			}

			RedsFileUtils.forceDelete(information.getServiceLocation());
			DeploymentContext deploymentInfo = getDeploymentInformation(name, version, information);
			service.unDeployed(deploymentInfo);
			commandResponse.setMessage("Service " + name + ":" + version + " undeployed");
			return commandResponse;
		} catch (ServiceException | RedsutilsException | ServiceManagementException e) {
			throw new PlatformCommandException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to un register deployed services", e);
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public CommandResponse logPath(LogsPathCmd cmd) throws PlatformCommandException {
		String serviceName = cmd.getServiceName();
		String serviceVersion = cmd.getServiceVersion();
		CommandResponse commandResponse = new CommandResponse<>();
		if (serviceName != null && serviceVersion != null) {
			PlatformLayout layout = RedsplatformcommonsTreasury.open.takePlatformLayout();
			String logPath = layout.getRoot() + "/" + "logs";
			commandResponse.setResponse(logPath);
		} else {
			PlatformLayout layout = RedsplatformcommonsTreasury.open.takePlatformLayout();
			String logPath = layout.getRoot() + "/" + "logs";
			commandResponse.setResponse(logPath);
		}
		commandResponse.setMessage("Log Path");

		return commandResponse;
	}

	private DeploymentContext getDeploymentInformation(String name, String version, ServiceInformation information) {
		DeploymentContext deploymentInfo = new DeploymentContext(information.getMetaData().getType(), name, version,
				information.getServiceLocation(), information.getServiceConfigLocation(), information.getServiceLibs());
		return deploymentInfo;
	}

	private void startloadedServices() throws ServiceManagementException {
		ServiceRegistryPersistance registryPersistance = PlatformmanagementTreasury.open
				.takeServiceRegistryPersistance();
		List<ServiceInfoPersistanceObject> persistenceObjects = registryPersistance.getAll();

		persistenceObjects.sort(new Comparator<ServiceInfoPersistanceObject>() {
			@Override
			public int compare(ServiceInfoPersistanceObject m1, ServiceInfoPersistanceObject m2) {
				if (m1.getInformation().getLastUpdatedTime() == m2.getInformation().getLastUpdatedTime()) {
					return 0;
				}
				if (m1.getInformation().getLastUpdatedTime() < m2.getInformation().getLastUpdatedTime()) {
					return -1;
				} else {
					return 1;
				}

			}
		});
		for (ServiceInfoPersistanceObject serv : persistenceObjects) {
			System.out.println("Service Order " + serv.getInformation().getMetaData().getName());
		}

		for (ServiceInfoPersistanceObject persistanceObject : persistenceObjects) {
			if (persistanceObject.getInformation().getStatus() == ServiceStatus.STARTED) {
				ServiceMetaData metaData = persistanceObject.getInformation().getMetaData();
				try {
					System.out.println("STARTING  service " + metaData.getName() + metaData.getVersion());
					PlatformmanagementTrack.me.info("STARTING  service {} : {} ", metaData.getName(),
							metaData.getVersion());
					StartCmd cmd = new StartCmd();
					cmd.setName(metaData.getName());
					cmd.setVersion(metaData.getVersion());
					startService(cmd);
					PlatformmanagementTrack.me.info("STARTED  service {} : {} ", metaData.getName(),
							metaData.getVersion());
				} catch (Exception e) {
					PlatformmanagementTrack.me.error("Failed to start service {} : {} ", metaData.getName(),
							metaData.getVersion(), e);
				}
			}
		}

	}

	public MarketPlaceManager getMarketPlaceManager() {
		return marketPlaceManager;
	}

}

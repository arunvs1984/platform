package com.reds.platform.platformmanagement.service;

import java.util.List;

import com.reds.platform.platformmanagement.exception.ServiceManagementException;
import com.reds.platform.redsplatformcommons.service.ServiceInformation;
import com.reds.platform.redsplatformcommons.service.ServiceStatus;
import com.reds.platform.servicecommons.Service;
import com.reds.platform.servicecommons.exception.ServiceException;

public interface ServiceRegistry {

	public void register(ServiceInformation metaData,boolean classLoadRequired) throws ServiceException;

	public ServiceInformation unRegister(String name, String version) throws ServiceException;

	public Service getService(String name, String version) throws ServiceException;

	public List<ServiceInformation> getAllServiceInfo() throws ServiceException;

	public void setStatus(ServiceStatus status, String name, String version) throws ServiceManagementException;
	
	public void load() throws ServiceException;

	ServiceRegistryEntry getServiceEntry(String name, String version) throws ServiceManagementException;
	
	public void printLoadedClass()throws ServiceManagementException;

	public void stop() throws ServiceManagementException;

}

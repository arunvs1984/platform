package com.reds.platform.platformmanagement.service.persistance;

import java.util.List;

import com.reds.library.redscommons.config.PlatformConfig;

public class PlatfromRepairObject implements PlatformConfig {

	private String id = "platformrepair";
	private List<String> librariesToRemove;

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getDescription() {
		return "Object to Hold Platform Repair Instructions";
	}

	public List<String> getLibrariesToRemove() {
		return librariesToRemove;
	}

	public void setLibrariesToRemove(List<String> librariesToRemove) {
		this.librariesToRemove = librariesToRemove;
	}

}

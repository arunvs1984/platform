package com.reds.platform.platformmanagement.service.persistance;

import com.reds.platform.platformmanagement.exception.ServiceManagementException;

public interface ServiceDependencyPersistence {
	
	public void save(ServiceDependencyPersistanceObject dependencyPersistanceObject) throws ServiceManagementException;

	public ServiceDependencyPersistanceObject getServiceDependencyPersistanceObject() throws ServiceManagementException;

}

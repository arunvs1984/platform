package com.reds.platform.platformmanagement.service.persistance;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.reds.library.redscommons.exception.RedscommonsException;
import com.reds.platform.platformmanagement.exception.PlatformmanagementErrorCodes;
import com.reds.platform.platformmanagement.exception.ServiceManagementException;
import com.reds.platform.platformmanagement.tracker.PlatformmanagementTrack;
import com.reds.platform.redsplatformcommons.config.AbstractConfigurationManager;
import com.reds.platform.redsplatformcommons.inject.RedsplatformcommonsTreasury;

@Singleton
public class ServieDependencyPersistenceImpl extends AbstractConfigurationManager
		implements ServiceDependencyPersistence {
	@Inject
	public ServieDependencyPersistenceImpl() {
		super(RedsplatformcommonsTreasury.open.takePlatformLayout().getServiceDependencyPersistance());
		try {
			super.initConfig(new ServiceDependencyPersistanceObject());
		} catch (RedscommonsException e) {
			PlatformmanagementTrack.me.error("Failed to initialize Service Dependency Persistance ",e);
		}

	}

	@Override
	public void save(ServiceDependencyPersistanceObject dependencyPersistanceObject) throws ServiceManagementException {
		try {
			super.write(dependencyPersistanceObject);
		} catch (RedscommonsException e) {
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to save dependency persistence metadata", e);
		}

	}

	@Override
	public ServiceDependencyPersistanceObject getServiceDependencyPersistanceObject()
			throws ServiceManagementException {
		try {
			// if(super.isExist(new ServiceDependencyPersistanceObject())) {
			return (ServiceDependencyPersistanceObject) super.read(new ServiceDependencyPersistanceObject());
			// }else {
			// return new ServiceDependencyPersistanceObject();
			// }

		} catch (RedscommonsException e) {
			throw new ServiceManagementException(PlatformmanagementErrorCodes.errorCode(1),
					"Failed to save dependency persistence metadata", e);
		}
	}

}

package com.reds.platform.redsplatformcommons.inject;

import com.reds.platform.redsplatformcommons.info.RedsplatformcommonsInfo;
import com.reds.platform.redsplatformcommons.layout.PlatformLayout;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerRedsplatformcommonsComponent implements RedsplatformcommonsComponent {
  private Provider<RedsplatformcommonsInfo> provideRedsplatformcommonsInfoProvider;

  private Provider<PlatformLayout> providePlatformLayoutProvider;

  private DaggerRedsplatformcommonsComponent(Builder builder) {
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static RedsplatformcommonsComponent create() {
    return new Builder().build();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {
    this.provideRedsplatformcommonsInfoProvider =
        DoubleCheck.provider(
            RedsplatformcommonsModule_ProvideRedsplatformcommonsInfoFactory.create(
                builder.redsplatformcommonsModule));
    this.providePlatformLayoutProvider =
        DoubleCheck.provider(
            RedsplatformcommonsModule_ProvidePlatformLayoutFactory.create(
                builder.redsplatformcommonsModule));
  }

  @Override
  public RedsplatformcommonsInfo takeRedsplatformcommonsInfo() {
    return provideRedsplatformcommonsInfoProvider.get();
  }

  @Override
  public PlatformLayout takePlatformLayout() {
    return providePlatformLayoutProvider.get();
  }

  public static final class Builder {
    private RedsplatformcommonsModule redsplatformcommonsModule;

    private Builder() {}

    public RedsplatformcommonsComponent build() {
      if (redsplatformcommonsModule == null) {
        this.redsplatformcommonsModule = new RedsplatformcommonsModule();
      }
      return new DaggerRedsplatformcommonsComponent(this);
    }

    public Builder redsplatformcommonsModule(RedsplatformcommonsModule redsplatformcommonsModule) {
      this.redsplatformcommonsModule = Preconditions.checkNotNull(redsplatformcommonsModule);
      return this;
    }
  }
}

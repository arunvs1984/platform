package com.reds.platform.redsplatformcommons.inject;

import com.reds.platform.redsplatformcommons.info.RedsplatformcommonsInfo;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RedsplatformcommonsModule_ProvideRedsplatformcommonsInfoFactory
    implements Factory<RedsplatformcommonsInfo> {
  private final RedsplatformcommonsModule module;

  public RedsplatformcommonsModule_ProvideRedsplatformcommonsInfoFactory(
      RedsplatformcommonsModule module) {
    this.module = module;
  }

  @Override
  public RedsplatformcommonsInfo get() {
    return provideInstance(module);
  }

  public static RedsplatformcommonsInfo provideInstance(RedsplatformcommonsModule module) {
    return proxyProvideRedsplatformcommonsInfo(module);
  }

  public static RedsplatformcommonsModule_ProvideRedsplatformcommonsInfoFactory create(
      RedsplatformcommonsModule module) {
    return new RedsplatformcommonsModule_ProvideRedsplatformcommonsInfoFactory(module);
  }

  public static RedsplatformcommonsInfo proxyProvideRedsplatformcommonsInfo(
      RedsplatformcommonsModule instance) {
    return Preconditions.checkNotNull(
        instance.provideRedsplatformcommonsInfo(),
        "Cannot return null from a non-@Nullable @Provides method");
  }
}

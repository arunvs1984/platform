package com.reds.platform.redsplatformcommons;

import com.reds.platform.redsplatformcommons.command.executor.PlatformCommandExecutor;
import com.reds.platform.redsplatformcommons.command.executor.ServiceCommandExecutor;

public enum CommandExecutors {
	INSTANCE;

	private PlatformCommandExecutor platformCommandExecutor;
	private ServiceCommandExecutor serviceCommandExecutor;

	public PlatformCommandExecutor getPlatformCommandExecutor() {
		return platformCommandExecutor;
	}

	public ServiceCommandExecutor getServiceCommandExecutor() {
		return serviceCommandExecutor;
	}

	public void init(PlatformCommandExecutor platformCommandExecutor, ServiceCommandExecutor serviceCommandExecutor) {
		if (null == this.platformCommandExecutor) {
			this.platformCommandExecutor = platformCommandExecutor;
		}

		if (null == this.serviceCommandExecutor) {
			this.serviceCommandExecutor = serviceCommandExecutor;
		}

	}

}

package com.reds.platform.redsplatformcommons.command;

/**
 * <b>Purpose:</b> Abstract base class for all platform commands
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public abstract class AbstractCommand {

	/**
	 * To hold the type of the command
	 */
	private CommandType commandType;

	/**
	 * @param commandType
	 */
	public AbstractCommand(CommandType commandType) {
		this.commandType = commandType;
	}

	final public CommandType getCommandType() {
		return commandType;
	}

}

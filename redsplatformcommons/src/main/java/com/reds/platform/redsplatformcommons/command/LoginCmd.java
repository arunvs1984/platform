package com.reds.platform.redsplatformcommons.command;

/**
 * <b>Purpose:</b> Command argument for Login Command
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public class LoginCmd extends AbstractCommand {

	/**
	 * To hold logged in user name
	 */
	private String userName;
	/**
	 * To hold logged in password
	 */
	private String password;
	/**
	 * Is remote login or not. Default is false. Indicates, login to current
	 * platform.If remote is true, means logging to remote platform.
	 */
	private boolean remote = false;
	/**
	 * Name of the platform. Only significant when remote login is true.
	 */
	private String nodeName;

	/**
	 * 
	 */
	public LoginCmd() {
		super(CommandType.LOGIN);
	}

	/**
	 * @return UserName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return
	 */
	public boolean isRemote() {
		return remote;
	}

	/**
	 * @param remote
	 */
	public void setRemote(boolean remote) {
		this.remote = remote;
	}

	/**
	 * @return
	 */
	public String getNodeName() {
		return nodeName;
	}

	/**
	 * @param nodeName
	 */
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

}

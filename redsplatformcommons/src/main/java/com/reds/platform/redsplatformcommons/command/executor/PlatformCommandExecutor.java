package com.reds.platform.redsplatformcommons.command.executor;

import com.reds.platform.redsplatformcommons.command.CommandResponse;
import com.reds.platform.redsplatformcommons.command.InfoCmd;
import com.reds.platform.redsplatformcommons.command.LoginCmd;
import com.reds.platform.redsplatformcommons.command.SystemInfoCmd;
import com.reds.platform.redsplatformcommons.exception.PlatformCommandException;

public interface PlatformCommandExecutor {

	public CommandResponse info(InfoCmd cmd) throws PlatformCommandException;

	public CommandResponse login(LoginCmd cmd) throws PlatformCommandException;

	public CommandResponse systemInfo(SystemInfoCmd cmd) throws PlatformCommandException;

	public CommandResponse runGc() throws PlatformCommandException;

}

package com.reds.platform.redsplatformcommons.command.executor;

import com.reds.platform.redsplatformcommons.command.CommandResponse;
import com.reds.platform.redsplatformcommons.command.DeployCmd;
import com.reds.platform.redsplatformcommons.command.ListCmd;
import com.reds.platform.redsplatformcommons.command.LogsPathCmd;
import com.reds.platform.redsplatformcommons.command.StartCmd;
import com.reds.platform.redsplatformcommons.command.StopCmd;
import com.reds.platform.redsplatformcommons.command.UnDeployCmd;
import com.reds.platform.redsplatformcommons.exception.PlatformCommandException;

public interface ServiceCommandExecutor {

	public CommandResponse listPlatformDeployedServices(ListCmd cmd) throws PlatformCommandException;
	
	public CommandResponse listMarketPlaceServices(ListCmd cmd) throws PlatformCommandException;

	/**
	 * Create tmp folder in side platform workarea location of platform.<br>
	 * Unzip zip folder inside the created tmp folder Read service.xml <br>
	 * Check if service type <br>
	 * If service type is Platform copy unzipped folder to redspot <br>
	 * If service type is Business copy unzipped folder to businesspot <br>
	 * Delete tmp folder Create ServiceRegistryEntry <br>
	 * Create ServiceClassLoader for deployed service <br>
	 * Load dependent libraries from lib folder of service using ServiceClassLoader
	 * <br>
	 * Load ServiceImplementation from servieJar using ServiceClassLoader and
	 * implementation class from sevice.xml
	 * 
	 * @param deployCmd
	 * @throws ServiceManagementException
	 */
	public CommandResponse deploy(DeployCmd cmd) throws PlatformCommandException;

	public CommandResponse UnDeploy(UnDeployCmd cmd) throws PlatformCommandException;

	public CommandResponse startService(StartCmd cmd) throws PlatformCommandException;

	public CommandResponse stopService(StopCmd cmd) throws PlatformCommandException;
	
	public CommandResponse logPath(LogsPathCmd cmd) throws PlatformCommandException;

}

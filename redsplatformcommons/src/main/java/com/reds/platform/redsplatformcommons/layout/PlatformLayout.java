package com.reds.platform.redsplatformcommons.layout;

import java.io.File;

import javax.inject.Singleton;

/**
 * <b>Purpose:</b>Platform layout class holds platform layout information of the
 * reds platform
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
@Singleton
public class PlatformLayout {

	private final String dot = ".";
	/**
	 * Root of platform is ./
	 */
	private  String root = dot + "/";

	/**
	 * Platform work area location ./.workarea/
	 */
	private  String workArea = root + "/" + ".workarea";

	/**
	 * Platform work area location ./.workarea/
	 */
	private  String serviceRegistryPersistance = workArea + "/" + "services";
	/**
	 * Platform work area location ./.workarea/
	 */
	private  String serviceDependencyPersistance = workArea + "/" + "dependency";

	/**
	 * Platform config folder's location ./config/
	 */
	private  String platformConfig = root + "/" + "config" + "/";
	
	/**
	 * Platform config folder's location ./config/
	 */
	private  String platformLib = root + "/" + "lib" + "/";
	/**
	 * Market place holds all service's zip, from which we can deploy to
	 * platform,Marketplace folder's location ./marketplace/
	 */
	private  String marketPlace = root + "/" + "marketplace" + "/";
	
	/**
	 * Market place holds all service's zip, from which we can deploy to
	 * platform,Marketplace folder's location ./marketplace/
	 */
	private  String marketPlaceIcon = root + "/" + "marketplace" + "/"+"icon"+ "/";
	
	private  String hangar = root + "/" + "hangar" + "/";	
	/**
	 * Platform service location ./redspot/
	 */
	private  String platformServiceRoot = root + "redspot" + "/";
	/**
	 * Platform service config folder's location ./redspot/config/
	 */
	private  String platformServiceConfig = "config" + "/";
	/**
	 * Platform service libs folder's location ./redspot/libs/
	 */
	private  String platformServiceLibs = "lib" + "/";
	/**
	 * Business service location ./businesspot/
	 */
	private  String businessServiceRoot = root + "businesspot" + "/";
	/**
	 * Business service config folder's location ./businesspot/config/
	 */
	private  String businessServiceConfig = "config" + "/";
	/**
	 * Business service libs folder's location ./businesspot/libs/
	 */
	private  String businessServiceLibs = "lib" + "/";
	/**
	 * service.xml metata Data xml which holds service information
	 */
	private  String servicMetaDataName = "service";

	public String getDot() {
		return dot;
	}

	public String getRoot() {
		return root;
	}

	public String getPlatformConfig() {
		return platformConfig;
	}

	public String getMarketPlace() {
		return marketPlace;
	}

	public String getPlatformServiceRoot() {
		return platformServiceRoot;
	}

	public String getPlatformServiceConfig() {
		return platformServiceConfig;
	}

	public String getPlatformServiceLibs() {
		return platformServiceLibs;
	}

	public String getBusinessServiceRoot() {
		return businessServiceRoot;
	}

	public String getBusinessServiceConfig() {
		return businessServiceConfig;
	}

	public String getBusinessServiceLibs() {
		return businessServiceLibs;
	}

	public String getServicMetaDataName() {
		return servicMetaDataName;
	}

	public String getWorkArea() {
		return workArea;
	}

	public final String getServiceRegistryPersistance() {
		return serviceRegistryPersistance;
	}

	public String getPlatformLib() {
		return platformLib;
	}

	public String getServiceDependencyPersistance() {
		return serviceDependencyPersistance;
	}

	public String getHangar() {
		return hangar;
	}

	public String getMarketPlaceIcon() {
		return marketPlaceIcon;
	}

	public void setRoot(String root) {
		this.root = root;
	}

	public void setWorkArea(String workArea) {
		this.workArea = workArea;
	}

	public void setServiceRegistryPersistance(String serviceRegistryPersistance) {
		this.serviceRegistryPersistance = serviceRegistryPersistance;
	}

	public void setServiceDependencyPersistance(String serviceDependencyPersistance) {
		this.serviceDependencyPersistance = serviceDependencyPersistance;
	}

	public void setPlatformConfig(String platformConfig) {
		this.platformConfig = platformConfig;
	}

	public void setPlatformLib(String platformLib) {
		this.platformLib = platformLib;
	}

	public void setMarketPlace(String marketPlace) {
		this.marketPlace = marketPlace;
	}

	public void setMarketPlaceIcon(String marketPlaceIcon) {
		this.marketPlaceIcon = marketPlaceIcon;
	}

	public void setHangar(String hangar) {
		this.hangar = hangar;
	}

	public void setPlatformServiceRoot(String platformServiceRoot) {
		this.platformServiceRoot = platformServiceRoot;
	}

	public void setPlatformServiceConfig(String platformServiceConfig) {
		this.platformServiceConfig = platformServiceConfig;
	}

	public void setPlatformServiceLibs(String platformServiceLibs) {
		this.platformServiceLibs = platformServiceLibs;
	}

	public void setBusinessServiceRoot(String businessServiceRoot) {
		this.businessServiceRoot = businessServiceRoot;
	}

	public void setBusinessServiceConfig(String businessServiceConfig) {
		this.businessServiceConfig = businessServiceConfig;
	}

	public void setBusinessServiceLibs(String businessServiceLibs) {
		this.businessServiceLibs = businessServiceLibs;
	}

	public void setServicMetaDataName(String servicMetaDataName) {
		this.servicMetaDataName = servicMetaDataName;
	}
	
	

}

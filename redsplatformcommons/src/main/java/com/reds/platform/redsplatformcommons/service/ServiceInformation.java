package com.reds.platform.redsplatformcommons.service;

public class ServiceInformation {
	private String serviceLocation;
	private String serviceLibs = null;
	private String serviceConfigLocation = null;
	private ServiceMetaData metaData;
	private ServiceStatus status = ServiceStatus.NOT_DEPLOYED;
	private long lastUpdatedTime;
	private String serviceIcon;

	public String getServiceLocation() {
		return serviceLocation;
	}

	public void setServiceLocation(String serviceLocation) {
		this.serviceLocation = serviceLocation;
	}

	public ServiceMetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(ServiceMetaData metaData) {
		this.metaData = metaData;
	}

	public final String getServiceLibs() {
		return serviceLibs;
	}

	public final void setServiceLibs(String serviceLibs) {
		this.serviceLibs = serviceLibs;
	}

	public final String getServiceConfigLocation() {
		return serviceConfigLocation;
	}

	public final void setServiceConfigLocation(String serviceConfigLocation) {
		this.serviceConfigLocation = serviceConfigLocation;
	}

	@Override
	public String toString() {
		return "\n"+this.status+this.metaData.toString();
	}

	public final ServiceStatus getStatus() {
		return status;
	}

	public final void setStatus(ServiceStatus status) {		
		this.status = status;
	}

	public final long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public final void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getServiceIcon() {
		return serviceIcon;
	}

	public void setServiceIcon(String serviceIcon) {
		this.serviceIcon = serviceIcon;
	}
	
	

}

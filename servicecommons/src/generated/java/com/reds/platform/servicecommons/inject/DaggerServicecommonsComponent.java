package com.reds.platform.servicecommons.inject;

import com.reds.platform.servicecommons.info.ServicecommonsInfo;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerServicecommonsComponent implements ServicecommonsComponent {
  private Provider<ServicecommonsInfo> provideServicecommonsInfoProvider;

  private DaggerServicecommonsComponent(Builder builder) {
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static ServicecommonsComponent create() {
    return new Builder().build();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {
    this.provideServicecommonsInfoProvider =
        DoubleCheck.provider(
            ServicecommonsModule_ProvideServicecommonsInfoFactory.create(
                builder.servicecommonsModule));
  }

  @Override
  public ServicecommonsInfo takeServicecommonsInfo() {
    return provideServicecommonsInfoProvider.get();
  }

  public static final class Builder {
    private ServicecommonsModule servicecommonsModule;

    private Builder() {}

    public ServicecommonsComponent build() {
      if (servicecommonsModule == null) {
        this.servicecommonsModule = new ServicecommonsModule();
      }
      return new DaggerServicecommonsComponent(this);
    }

    public Builder servicecommonsModule(ServicecommonsModule servicecommonsModule) {
      this.servicecommonsModule = Preconditions.checkNotNull(servicecommonsModule);
      return this;
    }
  }
}

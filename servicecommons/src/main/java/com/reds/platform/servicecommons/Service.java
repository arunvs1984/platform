package com.reds.platform.servicecommons;

import com.reds.platform.servicecommons.context.CloseContext;
import com.reds.platform.servicecommons.context.DeploymentContext;
import com.reds.platform.servicecommons.context.InitContext;
import com.reds.platform.servicecommons.context.ServiceContext;
import com.reds.platform.servicecommons.exception.ServiceException;

public interface Service {

	public void deployed(DeploymentContext deploymentInfo) throws ServiceException;

	public void init(InitContext context) throws ServiceException;

	public void start(ServiceContext context) throws ServiceException;

	public void close(CloseContext context) throws ServiceException;

	public void stop(ServiceContext context) throws ServiceException;

	public void unDeployed(DeploymentContext deploymentInfo) throws ServiceException;

}

package com.reds.platform.servicecommons;

import com.reds.platform.servicecommons.resilience.ResilienceCallBack;

public interface ServiceLookUp {
	public Service getService(String serviceName, String serviceVersion, ResilienceCallBack callBack);
}

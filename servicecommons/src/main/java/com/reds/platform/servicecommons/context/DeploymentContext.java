package com.reds.platform.servicecommons.context;

import com.reds.platform.redsplatformcommons.service.ServiceType;

public class DeploymentContext {
	private ServiceType type;
	private String name;
	private String version;
	private String serviceLocation;
	private String serviceConfigLocation;
	private String serviceLibLocation;
	/**
	 * @param type
	 * @param name
	 * @param version
	 * @param serviceLocation
	 * @param serviceConfigLocation
	 * @param serviceLibLocation
	 */
	public DeploymentContext(ServiceType type, String name, String version, String serviceLocation,
			String serviceConfigLocation, String serviceLibLocation) {
		this.type = type;
		this.name = name;
		this.version = version;
		this.serviceLocation = serviceLocation;
		this.serviceConfigLocation = serviceConfigLocation;
		this.serviceLibLocation = serviceLibLocation;
	}
	public final ServiceType getType() {
		return type;
	}
	public final void setType(ServiceType type) {
		this.type = type;
	}
	public final String getName() {
		return name;
	}
	public final void setName(String name) {
		this.name = name;
	}
	public final String getVersion() {
		return version;
	}
	public final void setVersion(String version) {
		this.version = version;
	}
	public final String getServiceLocation() {
		return serviceLocation;
	}
	public final void setServiceLocation(String serviceLocation) {
		this.serviceLocation = serviceLocation;
	}
	public final String getServiceConfigLocation() {
		return serviceConfigLocation;
	}
	public final void setServiceConfigLocation(String serviceConfigLocation) {
		this.serviceConfigLocation = serviceConfigLocation;
	}
	public final String getServiceLibLocation() {
		return serviceLibLocation;
	}
	public final void setServiceLibLocation(String serviceLibLocation) {
		this.serviceLibLocation = serviceLibLocation;
	}
	@Override
	public String toString() {
		return "DeploymentInfo [type=" + type + ", name=" + name + ", version=" + version + ", serviceLocation="
				+ serviceLocation + ", serviceConfigLocation=" + serviceConfigLocation + ", serviceLibLocation="
				+ serviceLibLocation + "]";
	}
	
}

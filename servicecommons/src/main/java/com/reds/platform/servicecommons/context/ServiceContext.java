package com.reds.platform.servicecommons.context;

import com.reds.platform.servicecommons.Service;
import com.reds.platform.servicecommons.ServiceLookUp;
import com.reds.platform.servicecommons.resilience.DefaultResilienceCallBack;
import com.reds.platform.servicecommons.resilience.ResilienceCallBack;

public class ServiceContext {

	private String configLocation;
	private ServiceLookUp lookUp;
	private String serviceName;
	private String serviceVersion;

	public ServiceContext(ServiceLookUp lookUp, String configLocation) {
		super();
		this.configLocation = configLocation;
		this.lookUp = lookUp;
	}

	public final String getConfigLocation() {
		return this.configLocation;
	}

	public final Service getService(String serviceName, String serviceVersion) {
		return lookUp.getService(serviceName, serviceVersion, new DefaultResilienceCallBack());
	}

	public final Service getService(String serviceName, String serviceVersion, ResilienceCallBack callBack) {
		return lookUp.getService(serviceName, serviceVersion, callBack);
	}

	public ServiceLookUp getLookUp() {
		return lookUp;
	}

	public void setLookUp(ServiceLookUp lookUp) {
		this.lookUp = lookUp;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceVersion() {
		return serviceVersion;
	}

	public void setServiceVersion(String serviceVersion) {
		this.serviceVersion = serviceVersion;
	}

}

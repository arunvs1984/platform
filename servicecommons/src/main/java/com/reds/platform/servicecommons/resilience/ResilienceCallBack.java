package com.reds.platform.servicecommons.resilience;

public interface ResilienceCallBack {

	public void onServiceNotFound(ResilienceObect resilienceObect);

	public void onServiceInvocationException(ResilienceObect resilienceObect, Exception exception);

}
